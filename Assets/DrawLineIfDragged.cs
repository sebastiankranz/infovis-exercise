using System.Collections;
using System.Collections.Generic;
using System.Linq;
using IATK;
using UnityEngine;

public class DrawLineIfDragged : MonoBehaviour
{
    public LineRenderer linePrefab;
    public List<LineRenderer> lines;
    private Timevis _timevis;
    public Transform anchor;

    // Start is called before the first frame update
    void Start()
    {
        _timevis = GetComponent<Timevis>();
    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.Two))
        {
            FindObjectsOfType<StateRepresentative>()
                .Where(it => _timevis.states.Contains(it.stateString))
                .ForEach(rep =>
                {
                    var line = Instantiate(linePrefab);
                    lines.Add(line);
                    line.SetPosition(0, rep.transform.position);
                });
        }

        if (OVRInput.GetUp(OVRInput.Button.Two))
        {
            lines.ForEach(it => it.gameObject.Destroy());
            lines.Clear();
        }

        lines.ForEach(line =>
        {
            line.SetPosition(1, anchor.position);
        });
    }
}