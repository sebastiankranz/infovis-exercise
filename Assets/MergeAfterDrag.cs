using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;

[RequireComponent(typeof(ProximityDragable))]
public class MergeAfterDrag : MonoBehaviour
{
    private List<MergeAfterDrag> overlappingObjects = new List<MergeAfterDrag>();

    [SerializeField] private GameObject mergeIndicator;
    [SerializeField] private Timevis timeVisPrefab;


    private ProximityDragable _dragable;

    // Start is called before the first frame update
    void Start()
    {
        mergeIndicator.SetActive(false);
        _dragable = GetComponent<ProximityDragable>();
        _dragable.OnDragEnd.TakeUntilDestroy(this).Subscribe(_ => MergeVisualization());
    }


    private void MergeVisualization()
    {
        if (overlappingObjects.Count == 0) return;

        var ownStates = GetComponent<Timevis>().states;
        var mergedStates
            = overlappingObjects
                .SelectMany(it => it.GetComponent<Timevis>().states)
                .Concat(ownStates)
                .Distinct()
                .ToList();

        overlappingObjects.ForEach(it => it.gameObject.Destroy());

        StartCoroutine(SpawnScatterplot(mergedStates));
    }

    private IEnumerator SpawnScatterplot(List<string> states)
    {
        var timeVis = GetComponent<Timevis>();

        var spawned = Instantiate(timeVisPrefab);
        spawned.data = timeVis.data;
        spawned.states = states;

        yield return null;

        spawned.transform.localScale = transform.localScale;
        spawned.transform.position = transform.position;
        spawned.transform.rotation = transform.rotation;
        gameObject.Destroy();
    }

    private void OnTriggerEnter(Collider other)
    {
        var mergeableObject = other.gameObject.GetComponent<MergeAfterDrag>();

        if (!mergeableObject) return;

        overlappingObjects.Add(mergeableObject);
        if (overlappingObjects.Count > 0) mergeIndicator.SetActive(true);
    }

    private void OnTriggerExit(Collider other)
    {
        var mergeableObject = other.gameObject.GetComponent<MergeAfterDrag>();
        if (!mergeableObject) return;

        overlappingObjects.Remove(mergeableObject);
        if (overlappingObjects.Count == 0) mergeIndicator.SetActive(false);
    }
}