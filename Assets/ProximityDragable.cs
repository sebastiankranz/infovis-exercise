using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

[RequireComponent(typeof(ProximityInteractible))]
public class ProximityDragable : MonoBehaviour
{
    private ProximityInteractible _interactible;

    public Subject<Unit> OnDragEnd = new Subject<Unit>();

    private Transform camera;

    // Start is called before the first frame update
    void Start()
    {
        _interactible = GetComponent<ProximityInteractible>();
        _interactible.onGrabbed.TakeUntilDestroy(this)
            .Subscribe(controller => StartDrag(controller));

        camera = Camera.main.transform;
    }

    void RotateAround(Vector3 pivotPoint, Quaternion rot)
    {
        transform.position = rot * (transform.position - pivotPoint) + pivotPoint;
        transform.rotation = rot * transform.rotation;
    }

    public void StartDrag(Transform controller) => StartCoroutine(DragCoroutine(controller));

    private IEnumerator DragCoroutine(Transform controller)
    {
        var offset = transform.position - controller.position;
        var rotationOffset = transform.rotation * Quaternion.Inverse(controller.rotation);

        while (OVRInput.Get(OVRInput.Button.Two))
        {
            transform.position = controller.position + offset;
            var direction = camera.position - transform.position;
            direction.y = 0;
            transform.rotation = Quaternion.LookRotation(-direction);

            yield return null;
        }

        OnDragEnd.OnNext(new Unit());
    }
}