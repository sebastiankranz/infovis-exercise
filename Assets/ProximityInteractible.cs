using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class ProximityInteractible : MonoBehaviour
{
    public readonly BehaviorSubject<bool> isHovered = new BehaviorSubject<bool>(false);
    public readonly Subject<Unit> onClicked = new Subject<Unit>();
    public readonly Subject<Transform> onGrabbed = new Subject<Transform>();

    public void Start()
    {
        FindObjectOfType<ProximityInteractionManager>().Register(this);
    }

    private void OnDestroy()
    {
        FindObjectOfType<ProximityInteractionManager>()?.Unregister(this);
    }
}
