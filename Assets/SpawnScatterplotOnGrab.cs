using System.Collections;
using System.Collections.Generic;
using IATK;
using UniRx;
using UnityEngine;

public class SpawnScatterplotOnGrab : MonoBehaviour
{
    [SerializeField] private Timevis visPrefab;
    [SerializeField] private CSVDataSource source;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<ProximityInteractible>().onGrabbed.TakeUntilDestroy(this)
            .Subscribe(c => StartCoroutine(StartGrab(c)));
    }

    private IEnumerator StartGrab(Transform controller)
    {
        var state = GetComponentInParent<StateRepresentative>().stateString;

        var spawned = Instantiate(visPrefab);
        spawned.data = source;
        spawned.states = new List<string>() {state};

        yield return null;

        spawned.transform.localScale = Vector3.one * 0.3f;
        var offset = spawned.transform.position -
                     spawned.GetComponentInChildren<AnchorPoint>().transform.position;
        spawned.transform.position = controller.position + offset;
        spawned.GetComponent<ProximityDragable>().StartDrag(controller);
    }
}