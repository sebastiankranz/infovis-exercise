using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IATK;
using System.Linq;
using TMPro;
using System;

public class Timevis : MonoBehaviour
{
    public CSVDataSource data;
    public List<string> states = new List<string>(new string[] {"Baden-Wuerttemberg"});

    [SerializeField] private GameObject labelTemplatez;
    [SerializeField] private GameObject AxisLabels;


    private static Hashtable stateColourValues = new Hashtable
    {
        {"Baden-Wuerttemberg", new Color(255, 0, 0, 1)},
        {"Bayern", new Color(0, 0, 255, 1)},
        {"Berlin", new Color(0, 255, 0, 1)},
        {"Brandenburg", new Color(0, 255, 255, 1)},
        {"Bremen", new Color(255, 0, 0, 1)},
        {"Hamburg", new Color(255, 0, 254, 1)},
        {"Hessen", new Color(255, 255, 0, 1)},
        {"Mecklenburg-Vorpommern", new Color(255, 255, 255, 1)},
        {"Niedersachsen", new Color(0, 0, 0, 1)},
        {"Nordrhein-Westfalen", new Color(0, 0, 255, 1)},
        {"Rheinland-Pfalz", new Color(0, 255, 0, 1)},
        {"Saarland", new Color(0, 255, 255, 1)},
        {"Sachsen", new Color(200, 100, 0, 1)},
        {"Sachsen-Anhalt", new Color(254, 0, 255, 1)},
        {"Schleswig-Holstein", new Color(254, 0, 0, 1)},
        {"Thueringen", new Color(254, 224, 255, 1)}
    };

    private object vis;

    void Start()
    {
        FacetBy(states);
    }

    public void FacetBy(List<string> states)
    {
        for (int i = 0; i < states.Count; i++)
        {
            View view = Facet(data, states[i], "Place", (Color) stateColourValues[states[i]]);
            view.transform.position = new Vector3(1, -0.683f, i * 0.1f);

            Transform labelz = Instantiate(labelTemplatez.transform);
            labelz.SetParent(AxisLabels.transform);
            labelz.gameObject.SetActive(true);
            labelz.position = new Vector3(1, 0.25f, i * 0.1f);
            labelz.GetChild(0).GetComponent<TMP_Text>().text = states[i];

            //GameObject labelZ = Instantiate(axisLabels.transform.Find("labelTemplatez").gameObject);
            //labelZ.SetParent(axisLabels.transform);
            //labelZ.gameObject.SetActive(true);
            //labelZ.anchoredPosition = new Vector3(0, i*0.5f, 0);
            //labelZ.GetChild(0).GetComponent<TMP_Text>().text = states[i];
        }
    }


    delegate float[] Filter(float[] ar, CSVDataSource csvds, string fiteredValue,
        string filteringAttribute);


    public View Facet(CSVDataSource csvds, string filteringValue, string filteringAttribute,
        Color color)
    {
        Filter baseFilter = (array, datasource, value, dimension) =>
        {
            return array.Select((b, i) => new {index = i, _base = b})
                .Where(b =>
                    datasource.getOriginalValuePrecise(csvds[dimension].Data[b.index], dimension)
                        .ToString() == value)
                .Select(b => b._base).ToArray();
        };


        Filter identity = (ar, ds, fv, fa) => { return ar; };
        // baseFilter = identity;

        var xData = baseFilter(csvds["Year"].Data, csvds, filteringValue, filteringAttribute);
        var yData = baseFilter(csvds["resultant"].Data, csvds, filteringValue, filteringAttribute);
        var zData = baseFilter(csvds["Place"].Data, csvds, filteringValue, filteringAttribute);
        ViewBuilder vb =
            new ViewBuilder(MeshTopology.Points, "Time Visualisation of Selected States")
                .initialiseDataView(xData.Length)
                .setDataDimension(xData, ViewBuilder.VIEW_DIMENSION.X)
                .setDataDimension(yData, ViewBuilder.VIEW_DIMENSION.Y)
                .setSize(Enumerable.Repeat(0.001f, yData.Length).ToArray())
                .setColors(zData.Select(x => color).ToArray());

        Material mt = IATKUtil.GetMaterialFromTopology(AbstractVisualisation.GeometryType.Points);
        mt.SetFloat("_Size", 0.15f);
        mt.SetFloat("_MinSize", 0.0001f);
        mt.SetFloat("_MaxSize", 0.001f);

        return vb.updateView().apply(gameObject, mt);
    }
}